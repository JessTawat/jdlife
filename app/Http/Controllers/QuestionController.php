<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request; 
use App\Http\Resources\Question as QuestionResource;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();

        return QuestionResource::collection($questions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $question = $request->isMethod('put') ? Question::findOrFail($request->$id) : new Question;

        $question->id = $request->input('id');
        $question->dropdrawingpaye = $request->input('dropdrawingpaye');
        $question->business = $request->input('business');
        $question->lengthofbusiness = $request->input('lengthofbusiness');
        $question->dropfulltime = $request->input('dropfulltime');
        $question->staffworking = $request->input('staffworking');
        $question->ontools = $request->input('ontools');
        $question->shareholdersnotontools = $request->input('shareholdersnotontools');
        $question->anualbnessincome = $request->input('anualbnessincome');
        $question->paymentfrequency = $request->input('paymentfrequency');
        $question->Acccover = $request->input('Acccover');
        $question->extracoveramount = $request->input('extracoveramount');
        //$question->save();

        if ($question->save())
        {
            return new QuestionResource($question);
            //return redirect()->route('collection', $question);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);

        if ($question->delete())
        {
            return new QuestionResource($question);
        }
    }
}
