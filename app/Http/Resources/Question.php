<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Question extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
        // return [
        //     'id' => $this->id,
        //     'dropdrawingpaye' => $this->dropdrawingpaye,
        //     'business' => $this->business,
        //     'lengthofbusiness' => $this->lengthofbusiness,
        //     'dropfulltime' => $this->dropfulltime,
        //     'staffworking' => $this->staffworking,
        //     'ontools' => $this->ontools,
        //     'shareholdersnotontools' => $this->shareholdersnotontools,
        //     'anualbnessincome' => $this->anualbnessincome,
        //     'paymentfrequency' => $this->paymentfrequency,
        //     'Acccover' => $this->Acccover, 
        //     'extracoveramount' => $this->extracoveramount, 
        // ];
    }
}
