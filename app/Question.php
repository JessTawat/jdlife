<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = "questions";
    protected $fillable = [
        'dropdrawingpaye',
        'business',
        'lengthofbusiness',
        'dropfulltime',
        'staffworking',
        'ontools',
        'shareholdersnotontools',
        'anualbnessincome',
        'Acccover',
        'extracoveramount'
    ];
}
