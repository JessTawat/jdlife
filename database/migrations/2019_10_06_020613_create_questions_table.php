<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dropdrawingpaye');
            $table->string('business');
            $table->integer('lengthofbusiness');
            $table->string('dropfulltime');
            $table->integer('staffworking');
            $table->string('ontools');
            $table->string('shareholdersnotontools');
            $table->double('anualbnessincome',8,2);
            $table->string('paymentfrequency');
            $table->string('Acccover');
            $table->double('extracoveramount',8,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
