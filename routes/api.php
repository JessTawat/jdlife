<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Collection of Question
Route::get('questions', 'QuestionController@index');
// single Question
// Route::get('question/{id}', 'QuestionController@show');
// create new Question
Route::post('question', 'QuestionController@store');
// update Question
//Route::put('question', 'QuestionController@store');
// delete
Route::delete('question/{id}', 'QuestionController@destroy');
